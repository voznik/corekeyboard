/*
	*
	* This file is a part of CoreKeyboard.
	* An on-screenkeyboard for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, see {http://www.gnu.org/licenses/}.
	*
*/

#pragma once

#include <QWidget>
#include <QMenu>
#include <QCoreApplication>
#include <QSystemTrayIcon>

#include "settings.h"

#include "corekeyboard.h"

class trayicon : public QSystemTrayIcon {

    Q_OBJECT

	public:
		trayicon( QWidget *parent ) : QSystemTrayIcon( parent ) {

            setIcon( QIcon::fromTheme( "org.cubocore.CoreKeyboard" ) );
			show();

			connect( this, SIGNAL( activated( QSystemTrayIcon::ActivationReason ) ), this, SLOT( activationHandler( QSystemTrayIcon::ActivationReason ) ) );

			QMenu *menu = new QMenu( "TrayMenu" );

			CoreKeyboard *kbd = qobject_cast<CoreKeyboard *>( parent );

			menu->addAction( QIcon(), "&Toggle Visible", kbd, SLOT( toggleShowHide() ) );
//            menu->addAction( QIcon(), "&Switch Layout", kbd, SLOT( switchLayout() ) );
			menu->addAction( QIcon(), "Switch &Mode", kbd, SLOT( switchMode() ) );
			menu->addSeparator();
			menu->addAction( QIcon::fromTheme( "application-quit" ), "&Quit CoreKeyboard", QCoreApplication::instance(), SLOT( quit() ) );

			setContextMenu( menu );
		}

	private Q_SLOTS:
		void activationHandler( QSystemTrayIcon::ActivationReason reason ) {

			if ( reason == QSystemTrayIcon::Trigger ) {
				settings smi;
				if ( ( int )smi.getValue("CoreApps", "UIMode") == 2 )
					contextMenu()->exec( QCursor::pos() );

				else
					emit toggleShowHide();
			}
		}

	Q_SIGNALS:
		void toggleShowHide();
		void switchLayout();
		void minimize();
};
